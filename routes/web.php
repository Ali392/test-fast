<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ProdukController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('dashboard.produk.index');
})->name('home');

// get data from api
Route::name('produk.')->group(function () {
    Route::get('/getData', [ProdukController::class, 'getData'])->name('getData');
    Route::get('/produk', [ProdukController::class, 'index'])->name('index');
    Route::post('/produk/store', [ProdukController::class, 'store'])->name('store');
    Route::get('/produk/{id}/edit', [ProdukController::class, 'edit'])->name('edit');
    Route::patch('/produk/update/{id}', [ProdukController::class, 'update'])->name('update');
    Route::get('/produk/getKategoriStatus', [ProdukController::class, 'getKategoriStatus'])->name('getKategoriStatus');
    Route::delete('/produk/delete/{id}', [ProdukController::class, 'destroy'])->name('delete');
});
