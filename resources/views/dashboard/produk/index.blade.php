@extends('layouts.dashboard-app')
@section('content')
<main id="main" class="main content">

    <div class="pagetitle">
      <h1>Produk</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
          <li class="breadcrumb-item active">Persediaan Produk</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">

			<div class="row mb-3">
				<!-- <div class="col-12 col-md-6">
					<div class="p-2 rounded bg-white shadow-sm">
						<div class="row">
							<label for="" class="col-sm-3 col-form-label">Filter Status</label>
							<div class="col-sm-9">
								<select class="form-select select-status" aria-label="Default select example">
									<option value="bisa dijual" selected>Bisa dijual</option>
									<option value="tidak bisa dijual">Tidak bisa dijual</option>
								</select>
							</div>
						</div>
					</div>
				</div> -->

				<div class="col">
					<div class="d-flex justify-content-end p-2">
						{{-- <a class="btn btn-success me-2" href="{{ route('produk.getData') }}" id="get-produk">
								<i class="bi bi-box-arrow-down me-1"></i>
								GET data
						</a> --}}
	
						<a class="btn btn-primary" href="#" id="add-produk">
								<i class="bi bi-plus-square me-1"></i>
								Tambah
						</a>
					</div>
				</div>
			</div>

      <div class="row">
        <div class="col">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Persediaan Produk</h5>

              <!-- Default Table -->
              <table class="table" id="datatable-produk">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Kategori</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Status</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
              <!-- End Default Table Example -->
            </div>
          </div>

        </div>
      </div>
    </section>

  </main>
@endsection

@section('script')
<script src="{{ asset('assets/module/produk/app.js') }}" type="module"></script>
@endsection