// for request data to controller
const requestData = (url, method, body) => {
   return fetch(url, {
      headers: {
         'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
         'X-Requested-With': 'XMLHttpRequest'
      },
      method,
      body,
   })
      .then(res => res.json())
      .then(res => {
         if (res.status != 200) {
            throw res
         }

         return res
      })
}

// format rupiah
const formatRupiah = (data) => new Intl.NumberFormat("id-ID", { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(data)


// reload datatable
const reloadTable = (table) => table.ajax.reload(null, false)

const strToCurr = (amount) => {
   let num = amount.replace(/\./g, '').replace(/\,/g, '.')
   num = parseFloat(num)
   return num
}

const alert = (type, title, text) => {
   toastr[type](text, title)
   toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": true,
      "progressBar": true,
      "positionClass": "toast-top-right",
      "preventDuplicates": true,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "300",
      "timeOut": "1500",
      "extendedTimeOut": "0",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
   }
}

// show error validation message to form
const showInvalidMessage = (formInput, invalid) => {
   const invalidFungsi = document.querySelector('.invalid-fungsi')
   const invalidJobdesk = document.querySelector('.invalid-jobdesk')
   if (invalidFungsi) invalidFungsi.innerHTML = ''
   if (invalidJobdesk) invalidJobdesk.innerHTML = ''

   const formControls = document.querySelectorAll(`.${formInput}`)
   formControls.forEach((control, i) => {
      control.classList.remove('is-invalid')

      for (const key in invalid) {
         if (control.getAttribute('type') == 'checkbox' && control.getAttribute('name') == `${key}[]`) {
            control.classList.add('is-invalid')
         }

         if (control.parentElement.classList.contains('input-group-table') && control.name == `${key}[]`) {
            control.classList.add('is-invalid')
            control.parentElement.parentElement.parentElement.parentElement.parentElement.nextElementSibling.innerHTML = `*${invalid[key][0]}`
         }

         if (control.getAttribute('type') != 'checkbox' && (control.getAttribute('name') == key || control.getAttribute('id') == key)) {
            control.classList.add('is-invalid')
            control.nextElementSibling.innerHTML = `*${invalid[key][0]}`
         }
      }
   })
}

const tooltip = () => {
   const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
   const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
}

const initDateTimePicker = () => {
   $('#tanggal').daterangepicker({
      "startDate": moment(),
      "endDate": moment(),
      locale: { format: "DD/MM/YYYY" },
   })
}

const initDateRangePicker = (obj) => {
   $(obj).daterangepicker({
      locale: { format: "DD/MM/YYYY" },
   })
}

const initDatePicker = (obj) => {

   $.fn.datepicker.dates['id'] = {
      days: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
      daysShort: ["Mgu", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Mgu"],
      daysMin: ["Mg", "Sn", "Sl", "Ra", "Ka", "Ju", "Sa", "Mg"],
      months: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
      monthsShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Ags", "Sep", "Okt", "Nov", "Des"],
      today: "Hari Ini",
      clear: "Kosongkan"
   };

   $(obj).datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
      language: 'id'
   });

}

// export module
export { requestData, initDatePicker, initDateRangePicker, strToCurr, formatRupiah, reloadTable, alert, showInvalidMessage, tooltip, initDateTimePicker }