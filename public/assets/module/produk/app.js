import { alert, reloadTable, requestData, showInvalidMessage } from "../global/global.js"
import { modalDeleteProduk, modalProduk } from "./menu-produk.js"

let tableProduk, isInsert, content, produkId
content = document.querySelector('.content')

tableProduk = $('#datatable-produk').DataTable({
        processing: true,
        serverSide: true,
        ajax: "/produk",
        // search: {
        //     "smart": false,
        //     // "regex": true,
        // },
        lengthMenu: [[25, 50, 100, -1], [25, 50, 100, "All"]],
        columns: [
            {data: 'DT_RowIndex', name: 'id_produk', orderable: false, searchable: false},
            {data: 'nama_produk', name: 'nama_produk'},
            {data: 'kategori_id', name: 'kategori_id'},
            {data: 'harga', name: 'harga'},
            {data: 'status_id', name: 'status_id'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        language: {
            lengthMenu: "_MENU_ Data per halaman",
            //zeroRecords: "Data tidak ditemukan",
            info: "Menampilkan halaman _PAGE_ dari _PAGES_",
            //infoEmpty: "Data kosong",
            infoFiltered: "(filter dari _MAX_ total data)",
            search: "Cari :",
    
            oPaginate: {
                sNext: '<i class="bi bi-arrow-right-short"></i>',
                sPrevious: '<i class="bi bi-arrow-left-short"></i>',
                sFirst: 'Pertama',
                sLast: 'Terakhir'
            },
        },
        dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-12'i><'col-12'<'d-flex justify-content-center'p>>>",
        drawCallback: () => {
            //show data
            // const btnShow = document.querySelectorAll('.show-data')
            // btnShow.forEach(btn => btn.addEventListener('click', async function () {
            //     this.disabled = true
            //     barangId = this.dataset.id

            //     const data = {
            //         title: 'Lihat Data Barang',
            //     }

            //     try {
            //         const result = await requestData(`barang/${barangId}/edit`, 'GET')
            //         data.barang = result.barang
    
            //         content.insertAdjacentHTML('afterend', modalShowBarang(data))
            //         const modalShow = document.getElementById('modal-show-barang')
            //         const modal = new bootstrap.Modal(modalShow)
            //         modal.show()

            //         modalShow.addEventListener('hidden.bs.modal', () => {
            //             modalShow.remove()
            //             this.disabled = false
            //         })
            //     } catch (error) {
            //         alert('error', 'Gagal', error.message)
            //     }
            // }))

            //edit data
            const btnEdit = document.querySelectorAll('.edit-data')
            btnEdit.forEach(btn => btn.addEventListener('click', async function (e) {
                this.disabled = true
                isInsert = false
                produkId = this.dataset.id

                const data = {
                    title: 'Edit Produk',
                }

                try {
                    const result = await requestData(`produk/${produkId}/edit`, 'GET')
                    data.produk = result.produk
                    data.kategories = result.kategories
                    data.statuses = result.statuses
    
                    content.insertAdjacentHTML('afterend', modalProduk(data))
                    const modalEdit = document.getElementById('modal-produk')
                    const modal = new bootstrap.Modal(modalEdit)
                    modal.show()
    
                    // batasi input angka
                    const inputNumber = document.querySelectorAll('.form-number')
                    inputNumber.forEach(input => input.addEventListener('input', function () {
                        this.value = this.value.replace(/[^0-9]/g, "")
                    }))
    
                    submit(isInsert, modal)
    
                    modalEdit.addEventListener('hidden.bs.modal', () => {
                        modalEdit.remove()
                        this.disabled = false
                    })
                } catch (error) {
                    alert('error', 'Gagal', error.message)
                }
            }))

            // Delete data
            const btnDelete = document.querySelectorAll('.delete-data')
            btnDelete.forEach(btn => btn.addEventListener('click', async function () {
                this.disabled = true
                produkId = this.dataset.id

                content.insertAdjacentHTML('afterend', modalDeleteProduk())
                const modalDelete = document.getElementById('delete-modal')
                const modal = new bootstrap.Modal(modalDelete)
                modal.show()
                const formDelete = document.querySelector('#form-delete-produk')
                formDelete.addEventListener('submit', async (e) => {
                    e.preventDefault()
                    modal.hide()
                    try {
                        const result = await requestData(`/produk/delete/${produkId}`, 'DELETE')
                        alert('success', 'Berhasil', result.message)
                    } catch (error) {
                        alert('error', 'Gagal', error.message) 
                    }

                    reloadTable(tableProduk)
                })
                modalDelete.addEventListener('hidden.bs.modal', () => {
                    modalDelete.remove()
                    this.disabled = false
                })
            }))
        }
    })

//filter data
// const selectStatus = document.querySelector('.select-status')
// selectStatus.addEventListener('change', function () {
//     tableProduk.column(4).search(this.value).draw()
// })

// tableProduk.search('bisa dijual').draw()

// $('.dataTables_filter input').unbind().bind('keyup', function() {
//     var searchTerm = this.value.toLowerCase(),
//         // regex = '\\b' + searchTerm + '\\b';
//         regex = '^\\s' + searchTerm +'\\s*$';
//         console.log(regex)
//         tableProduk.column(4).search(regex, true, false).draw();
//  })

//tambah produk
const btnAdd = document.querySelector('#add-produk')
btnAdd.addEventListener('click', async function (e) {
    btnAdd.disabled = true
    isInsert = true

    const data = {
        title: 'Tambah produk'
    }

    try {
        const result = await requestData('/produk/getKategoriStatus', 'GET')
        data.kategories = result.kategories
        data.statuses = result.statuses
        // console.log(result)

        content.insertAdjacentHTML('afterend', modalProduk(data))
        const modalAdd = document.getElementById('modal-produk')
        const modal = new bootstrap.Modal(modalAdd)
        modal.show()

        // batasi input angka
        const inputAngka = document.querySelectorAll('.form-number')
        inputAngka.forEach(input => input.addEventListener('input', function () {
            this.value = this.value.replace(/[^0-9]/g, "")
        }))

        submit(isInsert, modal)
        modalAdd.addEventListener('hidden.bs.modal', () => {
           modalAdd.remove()
           btnAdd.disabled = false
        })
    } catch (error) {
        alert('error', 'Gagal', error.message)
    }
  
})

const submit = (isInsert, modal) => {
    const btnSubmit = document.querySelector('#btn-submit')
    btnSubmit.addEventListener('click', async (e) => {
        e.preventDefault()

        const formData = new FormData()
        const formBarang = document.querySelectorAll('.form-produk')

        formBarang.forEach(form => formData.append(form.getAttribute('name'), form.value))

        let url
        
        if (isInsert) {
            url = '/produk/store'
        } else {
            url = `/produk/update/${produkId}`
            formData.append('_method', 'PATCH')
        }

        try {
            const result = await requestData(url, 'POST', formData)
            alert('success', 'Berhasil', result.message)
            modal.hide()
            reloadTable(tableProduk)
        } catch (error) {
            // error validation
            if (error.status == 422) {
                showInvalidMessage('form-produk', error.validation)
                return false
            }
 
            alert('error', 'Gagal', error.message)
        }
    })

}