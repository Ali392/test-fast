const modalProduk = (data) => {
   let content = `
   <div class="modal fade" id="modal-produk" role="dialog" tabindex="-1">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header text-bg-primary border-0">
               <h5 class="modal-title font-weight-normal">${data.title}</h5>
               <button class="btn-close text-white" data-bs-dismiss="modal" type="button" aria-label="Close"></button>
            </div>
            <div class="modal-body">
               <div class="mb-2">
                  <label for="nama" class="form-label">Nama Produk</label>
                  <input type="text" class="form-control form-produk" id="nama_produk" name="nama_produk" placeholder="" value="${data.produk ? data.produk.nama_produk : ''}">
                  <div class="invalid-feedback"></div>
               </div>

               <div class="mb-2">
									<label for="nama" class="form-label">Kategori Produk</label>
                  <select class="form-select form-produk" name="kategori_id" id="select_kategori">
                    <option value="">Pilih Kategori</option>`

										// append data
										data.kategories.forEach(kategori => {
												content += `<option value="${kategori.id_kategori}" ${data.produk ? (data.produk.kategori_id == kategori.id_kategori ? 'selected' : '') : ''}>${kategori.nama_kategori}</option>`
										})

										content += `
            			</select>
                  <div class="invalid-feedback"></div>
               </div>

               <label for="price" class="form-label">Harga Produk</label>
               <div class="input-group mb-2">
                  <span class="input-group-text">Rp</span>
                  <input type="text" class="form-control form-produk form-number" id="harga" name="harga" placeholder="" value="${data.produk ? data.produk.harga : ''}">
                  <div class="invalid-feedback"></div>
               </div>

							 <div class="mb-2">
									<label for="nama" class="form-label">Status Produk</label>
                  <select class="form-select form-produk" name="status_id" id="select_status">
                    <option value="">Pilih Status</option>`

										// append data
										data.statuses.forEach(status => {
												content += `<option value="${status.id_status}" ${data.produk ? (data.produk.status_id == status.id_status ? 'selected' : '') : ''}>${status.nama_status}</option>`
										})

										content += `
            			</select>
                  <div class="invalid-feedback"></div>
               </div>
            </div>
            <div class="modal-footer border-0">
               <button class="btn btn-secondary" data-bs-dismiss="modal" type="button">Cancel</button>
               <button class="btn btn-primary" type="submit" id="btn-submit">Submit</button>
            </div>
         </div>
      </div>
   </div>
   `

   return content
}

const modalDeleteProduk = () => {
	return `
	<div class="modal fade" id="delete-modal" tabindex="-1">
	<div class="modal-dialog w-25">
		 <div class="modal-content overflow-hidden">
				<div class="modal-body text-center">
					 <h5>Yakin data akan dihapus?</h6>
					 <p class="fs-6 mb-0">Anda tidak dapat mengembalikan data yang telah dihapus</p>
				</div>
				<form class="row justify-content-center align-items-center p-1 border-top" action="" method="" id="form-delete-produk">
					 <div class="col-6 border-end">
							<button type="submit" class="btn btn-sm text-danger btn-transparent w-100 fs-6">Hapus</button>
					 </div>
					 <div class="col-6">
							<button type="button" class="btn btn-sm text-secondary bg-transparent w-100 fs-6" data-bs-dismiss="modal">Batal</button>
					 </div>
				</form>
		 </div>
	</div>
</div>
 `
}

export { modalProduk, modalDeleteProduk }