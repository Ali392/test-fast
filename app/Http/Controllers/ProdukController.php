<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Kategori;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class ProdukController extends Controller
{
    public function getData() {
        $pass = md5('bisacoding-' . date('d-m-y'));
        $client = new \GuzzleHttp\Client();
        $response = $client->post(
            'https://recruitment.fastprint.co.id/tes/api_tes_programmer',
            array(
                'form_params' => array(
                    'username' => 'tesprogrammer111023C12',
                    'password' => $pass
                )
            )
        );

        //get data
        $data = json_decode($response->getBody());

        //save to database
        foreach($data->data as $key => $value) {
            $produk = new Produk;
            $produk->id_produk = $value->id_produk;
            $produk->nama_produk = $value->nama_produk;
            $produk->harga = $value->harga;
            $produk->kategori_id = $value->kategori;
            $produk->status_id = $value->status;
            $produk->save();
        }

        return redirect()->route('home');
    }

    public function index()
    {
        if (request()->ajax()) {
            $data = Produk::where('status_id', 1)->orderBy('id_produk', 'desc')->get();
            return DataTables::of($data)
              ->addIndexColumn()
              ->addColumn('action', function ($produk) {
                    return '
                        <div class="d-flex">
                          <button class="btn btn-warning btn-sm mx-1 edit-data" data-id="' . $produk->id_produk . '" type="button"><i class="bi bi-pencil"></i></button>
                          <button class="btn btn-danger btn-sm delete-data" data-id="' . $produk->id_produk . '" type="button"><i class="bi bi-trash"></i></button>
                        </div>
                        ';
                
              })
              ->editColumn('harga', function ($produk) {
                return 'Rp. ' . number_format($produk->harga, 0, ',', '.');
              })
              ->editColumn('status_id', function ($produk) {
                if($produk->status_id == 1) {
                    return "bisa dijual";
                } else {
                    return "tidak bisa dijual";
                }
              })
              ->editColumn('kategori_id', function ($produk) {
                return $produk->kategori->nama_kategori;
              })
              ->rawColumns(['action'])
              ->make('true');
        }

        // return redirect()->route('home);
    }

    public function getKategoriStatus() {
        if(request()->ajax()) {
            try {
                $kategories = Kategori::all();
                $statuses = Status::all();

                return ['status' => 200, 'kategories' => $kategories, 'statuses' => $statuses];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Gagal mengambil data!'];
            }
        }
    }

    public function store(Request $request)
    {
        if ($request->ajax()) {
            $rules = [
                'nama_produk' => 'required',
                'harga' => 'required',
                'kategori_id' => 'required',
                'status_id' => 'required',
            ];

            $messages = [
                'required' => ':attribute harus diisi',
            ];

            $aliases = [
                'name' => 'Nama produk',
                'harga' => 'Harga produk',
                'kategori_id' => 'Kategori',
                'status_id' => 'Status',
            ];

            $validator = Validator::make($request->all(), $rules, $messages, $aliases);

            if ($validator->fails()) {
                return ['status' => 422, 'validation' => $validator->getMessageBag()];
            }

            try {

                DB::transaction(function () use ($validator, $request) {
                    $validated = $validator->validated();
                    $barang = Produk::create($validated);
                });

                return ['status' => 200, 'message' => 'Data berhasil ditambahkan'];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Data gagal ditambahkan' . $th->getMessage()];
            }
        }
    }

    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {
            try {
                $produk = Produk::where('id_produk', $id)->first();
                $kategories = Kategori::all();
                $statuses = Status::all();

                return ['status' => 200, 'produk' => $produk, 'kategories' => $kategories, 'statuses' => $statuses];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Gagal mengambil data!'];
            }
        }
    }

    public function update(Request $request, $id)
    {
        if ($request->ajax()) {
            // $produk = Produk::where('id_produk', $id)->first();

            $rules = [
                'nama_produk' => 'required',
                'harga' => 'required',
                'kategori_id' => 'required',
                'status_id' => 'required',
            ];

            $messages = [
                'required' => ':attribute harus diisi',
            ];

            $aliases = [
                'name' => 'Nama produk',
                'harga' => 'Harga produk',
                'kategori_id' => 'Kategori',
                'status_id' => 'Status',
            ];

            $validator = Validator::make($request->all(), $rules, $messages, $aliases);

            if ($validator->fails()) {
                return ['status' => 422, 'validation' => $validator->getMessageBag()];
            }

            try {
                $validated = $validator->validated();
                Produk::where('id_produk', $id)->update([
                    'nama_produk' => $validated['nama_produk'],
                    'harga' => $validated['harga'],
                    'kategori_id' => $validated['kategori_id'],
                    'status_id' => $validated['status_id'],
                ]);

                return ['status' => 200, 'message' => 'Data berhasil diubah'];
            } catch (\Throwable $th) {
                return ['status' => 500, 'message' => 'Data gagal diubah' . $th->getMessage()];
            }
        }
    }

    public function destroy($id)
    {
        if (request()->ajax()) {
            try {
                Produk::where('id_produk', $id)->delete();
              return ['status' => 200, 'message' => 'Data berhasil dihapus'];
            } catch (\Throwable $th) {
              return ['status' => 500, 'message' => 'Data gagal dihapus'];
            }
        }
    }
}
